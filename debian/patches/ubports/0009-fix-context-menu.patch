Description: Make it possible to suppress QWE's touch context menu
 It seems like QWE's handling of touch selection caused apps to loss
 ability to customize context menu in touch case, as it seems to
 override the usual custom context menu path.
 .
 Use a custom, UT-specific environment variable to suppress QWE's touch
 selection menu, and let the usual context menu shows. If the
 environment variable is not set, then the QWE's touch selection menu
 comes up as it should (which is important if the app actually doesn't
 implement a custom context menu).
 .
 Upstream has already fixed this problem in Qt 6 by introducing a new
 signal for handling touch selection menu in addition to the original
 signal for context menu [1]. However, since moving to Qt 6 is a
 behemoth task in itself, let's have this workaround for now.
 .
 Credit goes to Maciej Sopylo <me@klh.io> for the original idea of
 disabling code around this area.
 .
 [1]: https://doc.qt.io/qt-6.5/qml-qtwebengine-webengineview.html#touchSelectionMenuRequested-signal
Author: Ratchanan Srirattanamet <ratchanan@ubports.com>
Origin: vendor
Bug: https://bugreports.qt.io/browse/QTBUG-85043
Bug-UBports: https://gitlab.com/ubports/development/core/packaging/qtwebengine/-/merge_requests/19
Bug-UBports: https://gitlab.com/ubports/development/core/packaging/qtwebengine/-/issues/16
Forwarded: not-needed
Last-Update: 2024-11-20
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/core/touch_selection_controller_client_qt.cpp
+++ b/src/core/touch_selection_controller_client_qt.cpp
@@ -70,8 +70,24 @@
 {
 }
 
+
+/* XXX: out-of-tree workaround of QTBUG-85043 until we get to upgrade to Qt 6.
+ * https://gitlab.com/ubports/development/core/packaging/qtwebengine/-/issues/16
+ */
+static bool touchSelectionMenuDisabled()
+{
+    static bool disabled = qEnvironmentVariableIsSet("QTWEBENGINE_DISABLE_TOUCH_SELECTION_MENU_UT");
+    return disabled;
+}
+
 bool TouchSelectionControllerClientQt::handleContextMenu(const content::ContextMenuParams& params)
 {
+    if (touchSelectionMenuDisabled()) {
+        /* XXX: don't call TouchSelectionController::HideAndDisallowShowingAutomatically(),
+         * we still want text selection handles. */
+        return false;
+    }
+
     if ((params.source_type == ui::MENU_SOURCE_LONG_PRESS ||
          params.source_type == ui::MENU_SOURCE_LONG_TAP) &&
         params.is_editable && params.selection_text.empty()) {
@@ -301,6 +317,9 @@
 
 void TouchSelectionControllerClientQt::showMenu()
 {
+    if (touchSelectionMenuDisabled())
+        return;
+
     gfx::RectF rect = GetTouchSelectionController()->GetRectBetweenBounds();
     gfx::PointF origin = rect.origin();
     gfx::PointF bottom_right = rect.bottom_right();
